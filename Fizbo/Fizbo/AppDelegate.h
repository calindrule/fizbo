//
//  AppDelegate.h
//  Fizbo
//
//  Created by Calin Drule on 16/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
