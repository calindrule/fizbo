//
//  DevTools.m
//  Fizbo
//
//  Created by Calin Drule on 28/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "DevTools.h"

// Public constant used for default GMT timezone
NSString * const GMT_TIMEZONE = @"GMT";

@implementation DevTools

+ (NSDate *)getDateFromString:(NSString*)dateString usingDateFormat:(NSString*)format
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // [formatter setLocale:enUSPOSIXLocale];
	[formatter setDateFormat:format];
	NSDate* date = [formatter dateFromString:dateString];
	return date;
}

+ (NSDate *)getDateInGMTFromString:(NSString*)dateString usingDateFormat:(NSString*)format
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:GMT_TIMEZONE]];
	[formatter setDateFormat:format];
	NSDate* date = [formatter dateFromString:dateString];
	return date;
}

@end
