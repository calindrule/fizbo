//
//  DevTools.h
//  Fizbo
//
//  Created by Calin Drule on 28/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DevTools : NSObject

// Returns a NSDate object from a given input string using the specified format
+ (NSDate *)getDateFromString:(NSString*)dateString usingDateFormat:(NSString*)format;

// Returns a NSDate object from a given input string in gmt timezone using the specified format
+ (NSDate *)getDateInGMTFromString:(NSString*)dateString usingDateFormat:(NSString*)format;

@end
