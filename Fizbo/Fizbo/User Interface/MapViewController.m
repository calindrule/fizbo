//
//  MapViewController.m
//  Fizbo
//
//  Created by Calin Drule on 16/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "MapViewController.h"
#import "RentPost.h"
#import "MapAnnotation.h"

@interface MapViewController () {
    BOOL mapWasCentered;
    
    MKMapView *map;
}

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - Private methods

// Method called to zoom and pan the map in order to display both the store and the user location
- (void)centerMap:(MKMapView *)view
{
    // The location where the map will be centered
	CLLocationCoordinate2D location;
	// Region of the map that will be visible after zooming
	MKCoordinateRegion region;
	// Span of the map region - determines the zoom
	MKCoordinateSpan span;
    
    // Center on user location
    location.latitude = view.userLocation.coordinate.latitude;
    location.longitude = view.userLocation.coordinate.longitude;
    
    // Set the new map center and span
	region.center = location;
	region.span = span;
	[view setRegion:region animated:TRUE];
    
    mapWasCentered = YES;
}

- (void)getPostingsInMap:(MKMapView*)mapView
{
    CLLocationCoordinate2D center = mapView.region.center;
    MKCoordinateSpan span = mapView.region.span;
    
    float minLat = center.latitude - span.latitudeDelta / 2;
    float maxLat = center.latitude + span.latitudeDelta / 2;
    float minLon = center.longitude - span.longitudeDelta / 2;
    float maxLon = center.longitude + span.longitudeDelta / 2;
    
    [[WSManager sharedInstance] getPostingsForLatitudeMin:minLat latitudeMax:maxLat longitudeMin:minLon longitudeMax:maxLon from:self];
}

#pragma mark - PostingsDelegate methods

- (void)postings:(NSArray*)postings arrivedWithSuccess:(BOOL)success
{
    if (!success) {
        return;
    }
    
    [map removeAnnotations:map.annotations];
    
    for (RentPost *post in postings) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(post.address.latitude, post.address.longitude);
        MapAnnotation *annotation = [[MapAnnotation alloc] initWithCoordinate:coordinate];
        [map addAnnotation:annotation];
    }
}

#pragma mark - MKMapViewDelegate methods

// This optional map view delegate method was implemented to allow us to change the map center and zoom
- (void)mapView:(MKMapView *)view didAddAnnotationViews:(NSArray *)views
{
	// pan and zoom the map to include both the store location and current user location
    if (!mapWasCentered) {
        [self centerMap:view];
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    map = mapView;
    
    // Cancel previous postings requests
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    // Wait for consecutive position updates
    [self performSelector:@selector(getPostingsInMap:) withObject:mapView afterDelay:1];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
}

- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error
{
    
}

- (MKAnnotationView *)mapView:(MKMapView *)view viewForAnnotation:(id<MKAnnotation>)annotation
{
	// Handle the user location
	if ([annotation isKindOfClass:[MKUserLocation class]]) {
		
		// Try to deque an existing pin view first
		MKPinAnnotationView *userPinView = (MKPinAnnotationView *)[view dequeueReusableAnnotationViewWithIdentifier:@"UserAnnotation"];
		
		if (!userPinView) {
			// If an existing pin view was not available, create one
			userPinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"UserAnnotation"];
			
			// Set the pin properties
			userPinView.pinColor = MKPinAnnotationColorGreen;
			userPinView.canShowCallout = TRUE;
		} else {
			// If the pin view was available, just update the associated annotation
			userPinView.annotation = annotation;
		}
		
		// Return the pin view representing the current user location
		return userPinView;
	}
	
	// Handle a screen location
	if ([annotation isKindOfClass:[MapAnnotation class]]) {
		
		// Try to deque an existing pin view first
		MKPinAnnotationView *pinView = (MKPinAnnotationView *)[view dequeueReusableAnnotationViewWithIdentifier:@"MapAnnotation"];
		
		if (!pinView) {
			// If an existing pin view was not available, create one
			pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MapAnnotation"];
			
			// Set the pin properties
			pinView.pinColor = MKPinAnnotationColorRed;
			pinView.canShowCallout = TRUE;
		} else {
			// If the pin view was available, just update the associated annotation
			pinView.annotation = annotation;
		}
		// Return the pin view representing the posting address
		return pinView;
	}
	
	// Display nothing.
	return nil;
}


@end
