//
//  UserPostingsViewController.h
//  Fizbo
//
//  Created by Calin Drule on 17/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <UIKit/UIKit.h>

#define STORYBOARD_USER_POSTINGS_ID @"UserPostingsViewController"

@interface UserPostingsViewController : UIViewController

@end
