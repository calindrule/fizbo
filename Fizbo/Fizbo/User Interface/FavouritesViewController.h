//
//  FavouritesViewController.h
//  Fizbo
//
//  Created by Calin Drule on 17/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserPostingsViewController.h"

#define STORYBOARD_FAVOURITES_ID @"FavouritesViewController"

@interface FavouritesViewController : UserPostingsViewController

@end
