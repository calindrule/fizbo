//
//  MenuView.m
//  Fizbo
//
//  Created by Calin Drule on 16/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "MenuView.h"

@interface MenuView () {
    NSMutableArray *menuOptions;
}

@property (nonatomic, retain) IBOutlet UITableView *table;

@end

@implementation MenuView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        
        NSArray *optionTypes = @[@(MenuTypeLogin), @(MenuTypeMap), @(MenuTypeMyPostings), @(MenuTypeFavourites), @(MenuTypeProfile)];
        
        menuOptions = [NSMutableArray new];
        
        for (NSNumber *menuOptionType in optionTypes) {
            MenuOption *menuOption = [[MenuOption alloc] initWithType:[menuOptionType intValue]];
            [menuOptions addObject:menuOption];
        }
    }
    
    return self;
}

#pragma mark - Custom accessor methods

- (void)setTable:(UITableView *)table
{
    _table = table;
    table.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuOptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    
    MenuOption *menuOption = [menuOptions objectAtIndex:indexPath.row];
    cell.textLabel.text = menuOption.name;
    
    return cell;
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuOption *menuOption = [menuOptions objectAtIndex:indexPath.row];
    
    [self.delegate selectedMenuOption:menuOption.type];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
