//
//  MapViewController.h
//  Fizbo
//
//  Created by Calin Drule on 16/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "WSManager+Postings.h"

#define STORYBOARD_MAP_ID @"MapViewController"

@interface MapViewController : UIViewController <PostingsDelegate, MKMapViewDelegate>

@end
