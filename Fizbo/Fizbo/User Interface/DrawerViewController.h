//
//  ViewController.h
//  Fizbo
//
//  Created by Calin Drule on 16/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ControlBar.h"
#import "MenuView.h"

@interface DrawerViewController : UIViewController <ControlBarDelegate, MenuViewDelegate>

@end
