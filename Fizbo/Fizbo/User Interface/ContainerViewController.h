//
//  ContainerViewController.h
//  Fizbo
//
//  Created by Calin Drule on 18/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerViewController : UIViewController

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController;

- (void)swapToViewController:(UIViewController *)toViewController;

@end
