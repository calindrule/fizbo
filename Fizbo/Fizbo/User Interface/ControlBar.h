//
//  ControlBar.h
//  Fizbo
//
//  Created by Calin Drule on 16/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ControlBarDelegate <NSObject>

- (void)openMenuView;

- (void)openSearchView;

- (void)openAddView;

@end

@interface ControlBar : UIView

@property (nonatomic, assign) id <ControlBarDelegate> delegate;

@end
