//
//  ControlBar.m
//  Fizbo
//
//  Created by Calin Drule on 16/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "ControlBar.h"

@interface ControlBar () {
    IBOutlet UIButton *menuButton;
    
    IBOutlet UIButton *searchButton;
    
    IBOutlet UIButton *addButton;
}

@end

@implementation ControlBar

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - User Actions

- (IBAction)menuButtonTapped:(id)sender
{
    [delegate openMenuView];
}

- (IBAction)searchButtonTapped:(id)sender
{
    
}

- (IBAction)addButtonTapped:(id)sender
{
    
}

@end
