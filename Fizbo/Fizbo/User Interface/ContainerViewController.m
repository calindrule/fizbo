//
//  ContainerViewController.m
//  Fizbo
//
//  Created by Calin Drule on 18/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "ContainerViewController.h"

#define SegueIdentifierMap @"embedMap"
#define SegueIdentifierPostings @"embedPostings"
#define SegueIdentifierFavourites @"embedFavourites"
#define SegueIdentifierProfile @"embedProfile"
#define SegueIdentifierLogin @"embedLogin"

@interface ContainerViewController ()

@property (strong, nonatomic) NSString *currentSegueIdentifier;

@end


@implementation ContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentSegueIdentifier = SegueIdentifierMap;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (self.childViewControllers.count > 0) {
        [self swapToViewController:segue.destinationViewController];
    }
    else {
        [self addChildViewController:segue.destinationViewController];
        ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
        [segue.destinationViewController didMoveToParentViewController:self];
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
    }];
}

- (void)swapToViewController:(UIViewController *)toViewController
{
    if (toViewController) {
        [self swapFromViewController:[self.childViewControllers firstObject] toViewController:toViewController];
    }
}


@end
