//
//  MenuView.h
//  Fizbo
//
//  Created by Calin Drule on 16/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuOption.h"

@protocol MenuViewDelegate <NSObject>

- (void)selectedMenuOption:(MenuOptionType)option;

@end

@interface MenuView : UIView <UITableViewDataSource, UITableViewDelegate> {
    
}

@property (nonatomic, assign) id <MenuViewDelegate> delegate;

@end
