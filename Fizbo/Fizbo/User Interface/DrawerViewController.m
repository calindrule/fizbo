//
//  ViewController.m
//  Fizbo
//
//  Created by Calin Drule on 16/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "DrawerViewController.h"
#import "MapViewController.h"
#import "UserPostingsViewController.h"
#import "FavouritesViewController.h"
#import "UserProfileViewController.h"
#import "ContainerViewController.h"
#import "LoginViewController.h"

@interface DrawerViewController () {
    IBOutlet ControlBar *controlBar;
    
    IBOutlet MenuView *menuView;
    
    IBOutlet NSLayoutConstraint *menuLeadingConstraint;
    
    IBOutlet UIView *shadowView;
    
    ContainerViewController *containerController;
}

@end

@implementation DrawerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    controlBar.delegate = self;
    
    menuView.delegate = self;
    
    menuLeadingConstraint.constant = -self.view.frame.size.width;
    
    containerController = [self.childViewControllers firstObject];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

#pragma mark - ControlBarDelegate methods

- (void)openMenuView
{
    float leadingConstant = 0;
    float shadowAlpha = 0;
    
    if (menuLeadingConstraint.constant == 0) {
        // Hide menu
        leadingConstant = - self.view.frame.size.width;
        shadowAlpha = 0;
    }
    else {
        // Show menu
        leadingConstant = 0;
        shadowAlpha = 0.5;
    }
    
    shadowView.hidden = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        menuLeadingConstraint.constant = leadingConstant;
        shadowView.alpha = shadowAlpha;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (shadowAlpha == 0) {
            shadowView.hidden = YES;
        }
    }];
}

- (void)openSearchView
{
    
}

- (void)openAddView
{
    
}

#pragma mark - MenuViewDelegate methods

- (void)selectedMenuOption:(MenuOptionType)option
{
    
    UIViewController *destinationController = nil;
    switch (option) {
        case MenuTypeLogin: {
            destinationController = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_LOGIN_ID];
            break;
        }
        case MenuTypeMap: {
            destinationController = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_MAP_ID];
            break;
        }
        case MenuTypeMyPostings: {
            destinationController = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_USER_POSTINGS_ID];
            
            break;
        }
        case MenuTypeFavourites: {
            destinationController = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_FAVOURITES_ID];
            break;
        }
        case MenuTypeProfile: {
            destinationController = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_USER_PROFILE_ID];
            break;
        }
        default:
            break;
    }
    
    [containerController swapToViewController:destinationController];
    
    [self openMenuView];
}

@end
