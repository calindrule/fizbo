//
//  WSManager+Postings.h
//  Fizbo
//
//  Created by Calin Drule on 25/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "WSManager.h"

@protocol PostingsDelegate <NSObject>

- (void)postings:(NSArray*)postings arrivedWithSuccess:(BOOL)success;

@end

@interface WSManager (Postings)

- (void)getPostingsForLatitudeMin:(float)latMin latitudeMax:(float)latMax longitudeMin:(float)lonMin longitudeMax:(float)lonMax from:(id<PostingsDelegate>)delegate;

@end
