//
//  WSManager.m
//  Fizbo
//
//  Created by Calin Drule on 23/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "WSManager.h"

@implementation WSManager

+ (WSManager*)sharedInstance
{
    static WSManager *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[WSManager alloc] init];
    });
    return _sharedInstance;
}

- (void)getMethodWith:(NSString *)URLString success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    [manager GET:URLString parameters:nil success:success failure:failure];
}

@end
