//
//  PostingsTranslator.h
//  Fizbo
//
//  Created by Calin Drule on 26/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "BaseTranslator.h"

@interface PostingsTranslator : BaseTranslator

+ (NSArray*)postingsFromDictionary:(NSDictionary*)dictionary;

@end
