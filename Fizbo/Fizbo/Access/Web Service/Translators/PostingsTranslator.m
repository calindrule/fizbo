//
//  PostingsTranslator.m
//  Fizbo
//
//  Created by Calin Drule on 26/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "PostingsTranslator.h"
#import "RentPost.h"
#import "DevTools.h"

#define RENTS_KEY @"rents"
#define RENT_ID_KEY @"rentId"
#define ADDRESS_KEY @"address"
#define ADDRESS_ID_KEY @"addressId"
#define ADDRESS_LATITUDE_KEY @"addressLatitude"
#define ADDRESS_LONGITUDE_KEY @"addressLongitude"
#define ADDRESS_NEIGHBURHOOD_KEY @"addressNeighbourhood"
#define ADDRESS_STREET_KEY @"addressStreetName"
#define PRICE_KEY @"rentPrice"
#define AREA_KEY @"rentSurface"
#define ROOMS_KEY @"rentRooms"
#define BATHS_KEY @"rentBaths"
#define TYPE_KEY @"rentType"
#define ARCHITECTURE_KEY @"rentArchitecture"
#define ADD_DATE_KEY @"rentAddDate"
#define STATUS_KEY @"rentStatus"
#define IMAGES_KEY @"rentImages"

#define DATE_FORMAT @"MMM DD, YYYY hh:mm:ss a"

@implementation PostingsTranslator

+ (NSArray*)postingsFromDictionary:(NSDictionary*)dictionary
{
    NSMutableArray *postings = [[NSMutableArray alloc] init];
    
    NSArray *postingsArray = [dictionary objectForKey:RENTS_KEY];
    
    for (NSDictionary *postingDictionary in postingsArray) {
        RentPost *posting = [RentPost new];
        
        PostAddress *address = [PostAddress new];
        NSDictionary *addressDictionary = [postingDictionary objectForKey:ADDRESS_KEY];
        address.addressId = [[addressDictionary objectForKey:ADDRESS_ID_KEY] integerValue];
        address.latitude = [[addressDictionary objectForKey:ADDRESS_LATITUDE_KEY] doubleValue];
        address.longitude = [[addressDictionary objectForKey:ADDRESS_LONGITUDE_KEY] doubleValue];
        address.neighbourhood = [addressDictionary objectForKey:ADDRESS_NEIGHBURHOOD_KEY];
        address.street = [addressDictionary objectForKey:ADDRESS_STREET_KEY];
        posting.address = address;
        
        posting.postId = [[postingDictionary objectForKey:RENT_ID_KEY] integerValue];
        posting.rentPrice = [[postingDictionary objectForKey:PRICE_KEY] integerValue];
        posting.area = [[postingDictionary objectForKey:AREA_KEY] floatValue];
        posting.roomsNo = [[postingDictionary objectForKey:ROOMS_KEY] integerValue];
        posting.bathsNo = [[postingDictionary objectForKey:BATHS_KEY] integerValue];
        posting.rentType = [[postingDictionary objectForKey:TYPE_KEY] integerValue];
        posting.rentArchitecture = [[postingDictionary objectForKey:ARCHITECTURE_KEY] integerValue];
        posting.addDate = [DevTools getDateInGMTFromString:[postingDictionary objectForKey:ADD_DATE_KEY] usingDateFormat:DATE_FORMAT];
        posting.rentStatus = [[postingDictionary objectForKey:STATUS_KEY] integerValue];
        
        NSArray *imageURLs = [postingDictionary objectForKey:IMAGES_KEY];
        
        posting.imageURLs = imageURLs;
        
        [postings addObject:posting];
    }
    
    return postings;
}

@end
