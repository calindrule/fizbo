//
//  WSManager.h
//  Fizbo
//
//  Created by Calin Drule on 23/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

@interface WSManager : NSObject

+ (WSManager*)sharedInstance;

- (void)getMethodWith:(NSString *)URLString success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
