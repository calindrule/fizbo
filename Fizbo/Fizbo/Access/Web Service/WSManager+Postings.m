//
//  WSManager+Postings.m
//  Fizbo
//
//  Created by Calin Drule on 25/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "WSManager+Postings.h"
#import "PostingsTranslator.h"

@implementation WSManager (Postings)

- (void)getPostingsForLatitudeMin:(float)latMin latitudeMax:(float)latMax longitudeMin:(float)lonMin longitudeMax:(float)lonMax from:(id<PostingsDelegate>)delegate
{
    NSString *urlString = [NSString stringWithFormat:@"http://test.fizbo.ro/ws/rents/map?minLatitude=%f&maxLatitude=%f&minLongitude=%f&maxLongitude=%f&pageSize=20", latMin, latMax, lonMin, lonMax];
    
    [self getMethodWith:urlString success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *postings = [PostingsTranslator postingsFromDictionary:responseObject];
        
        [delegate postings:postings arrivedWithSuccess:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [delegate postings:nil arrivedWithSuccess:NO];
    }];
}
@end
