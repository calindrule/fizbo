//
//  RentPost.h
//  Fizbo
//
//  Created by Calin Drule on 27/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostAddress.h"

typedef enum RentTypes {
    RentNotSet          = 0,
	RentRealEstate      = 1
} RentType;

typedef enum RentArchitectures {
    ArchitectureNotSet      = 0,
    ArchitectureFlat        = 1
} RentArchitecure;

typedef enum RentStatuses {
    StatusNotSet            = 0,
    StatusPosted            = 1
} RentStatus;

@interface RentPost : NSObject

@property (nonatomic) NSInteger postId;
@property (nonatomic) NSInteger rentPrice;
@property (nonatomic) float area;
@property (nonatomic) NSInteger roomsNo;
@property (nonatomic) NSInteger bathsNo;
@property (nonatomic) NSDate *addDate;
@property (nonatomic, strong) NSArray *imageURLs;
@property (nonatomic) RentType rentType;
@property (nonatomic) RentArchitecure rentArchitecture;
@property (nonatomic) RentStatus rentStatus;
@property (nonatomic, strong) PostAddress *address;

@end
