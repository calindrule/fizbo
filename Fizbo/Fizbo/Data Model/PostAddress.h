//
//  PostAddress.h
//  Fizbo
//
//  Created by Calin Drule on 27/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostAddress : NSObject

@property (nonatomic) NSInteger addressId;
@property (nonatomic, copy) NSString *street;
@property (nonatomic, copy) NSString *neighbourhood;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@end
