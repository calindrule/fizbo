//
//  MapAnnotation.m
//  Fizbo
//
//  Created by Calin Drule on 02/09/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "MapAnnotation.h"

@interface MapAnnotation () {
    NSString *title;
}

@end

@implementation MapAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    self = [super init];
    if (self) {
        _coordinate = coordinate;
    }
    
    return self;
}

- (NSString *)title
{
    return title;
}

@end
