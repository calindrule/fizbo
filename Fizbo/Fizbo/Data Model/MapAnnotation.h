//
//  MapAnnotation.h
//  Fizbo
//
//  Created by Calin Drule on 02/09/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnnotation : NSObject <MKAnnotation>

// Property used by the Map Kit framework to locate the annotation view on the map
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

// Retrieve the title of the callout
- (NSString *)title;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
