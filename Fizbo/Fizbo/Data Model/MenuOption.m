//
//  MenuOption.m
//  Fizbo
//
//  Created by Calin Drule on 17/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import "MenuOption.h"


@implementation MenuOption

@synthesize name;

- (instancetype)initWithType:(MenuOptionType)type
{
    self = [super init];
    if (self) {
        _type = type;
        
        switch (type) {
            case MenuTypeLogin:
                name = NSLocalizedString(@"menuView.login.text", nil);
                break;
            case MenuTypeMap:
                name = NSLocalizedString(@"menuView.map.text", nil);
                break;
            case MenuTypeMyPostings:
                name = NSLocalizedString(@"menuView.myPostings.text", nil);
                break;
            case MenuTypeFavourites:
                name = NSLocalizedString(@"menuView.favourites.text", nil);
                break;
            case MenuTypeProfile:
                name = NSLocalizedString(@"menuView.profile.text", nil);
                break;
            default:
                break;
        }
    }
    
    return self;
}

@end
