//
//  MenuOption.h
//  Fizbo
//
//  Created by Calin Drule on 17/08/14.
//  Copyright (c) 2014 Fizbo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum MenuOptionTypes {
    MenuTypeNotSet      = 0,
    MenuTypeLogin       = 1,
    MenuTypeMap         = 2,
    MenuTypeMyPostings  = 3,
    MenuTypeFavourites  = 4,
    MenuTypeProfile     = 5
} MenuOptionType;

@interface MenuOption : NSObject

@property (nonatomic, readonly) MenuOptionType type;

@property (nonatomic, copy) NSString *name;

- (instancetype)initWithType:(MenuOptionType)type;

@end
